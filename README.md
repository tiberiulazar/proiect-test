# JS training

Presequels:
* NodeJS ([Homepage - Download the LTS version](https://nodejs.org/en/))
* Git ([Homepage](https://git-scm.com/))
* VSCode  ([Homepage](https://code.visualstudio.com/))

execute:
1. ``git clone https://gitlab.com/tudorie.mariuscosmin/javascript-training`` 
1. ``cd javascript-training`` (intram in folder-ul mare)
1. ``cd public`` (intram in folder-ul de front-end)
1. ``code .`` (deschidem in VSCode folderul cu ce este in front)
1. ``cd ..`` (ne intoarcem in folder-ul mare)
1. ``npm install`` (instalam dependitele pentru server)
1. ``npm start`` (pornim server-ul)



#### Useful links:

* Axios: https://github.com/axios/axios
* Toastr: https://github.com/CodeSeven/toastr
* jQuery: https://developers.google.com/speed/libraries/



regex for numerical only: ``/^[0-9]*$/``